package org.aoc
import org.aoc.data.AoC
import org.aoc.days.*

fun main() {
    val testInput = false
    listOf<AoC<*, *, *>>(
//        Day3(),
//        Day6(),
//        Day7(),
//        Day8(),
//        Day9(),
//        Day10(),
//        Day12(),
//        Day13(),
//        Day14(),
//        Day15(),
        Day25()
    )
        .forEach { dailyTask ->
            val executedDailyTask = runWithPerformanceCheck { dailyTask.execute(testInput) }
            println("Day${dailyTask.day} full execution time: ${executedDailyTask.executionTime}ms")
            println()
            executedDailyTask.solution.printSolution()
            println()
        }
}
