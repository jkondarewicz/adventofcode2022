package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.abs

class Day9: AoC<List<RopeMove>, Int, Int>() {
    override fun part1(parsedData: List<RopeMove>): Int = track(parsedData, 1)

    override fun part2(parsedData: List<RopeMove>): Int = track(parsedData, 9)

    override fun parseInput(input: String, step: Int): List<RopeMove> =
        input
            .split("\n")
            .map { line ->
                val cmd = line.split(" ")
                val dir = Direction.createFrom(cmd[0])
                val q = cmd[1].toInt()
                RopeMove(dir, q)
            }

    private fun track(ropeMoves: List<RopeMove>, which: Int): Int {
        var head = Pos(0, 0)
        val tails = mutableListOf<Pos>()
        val visited = mutableListOf<MutableSet<String>>()
        for(i in 0 until which) {
            tails.add(Pos(0, 0))
            visited.add(mutableSetOf())
        }
        ropeMoves.forEach { ropeMove ->
            for(i in 0 until ropeMove.howMany) {
                head = head.move(ropeMove.direction.pos)
                tails[0] = tails[0].track(head)
                visited[0].add(tails[0].key)
                for(j in 1 until which) {
                    tails[j] = tails[j].track(tails[j-1])
                    visited[j].add(tails[j].key)
                }
            }
        }
        return visited[which - 1].size
    }
}

enum class Direction(val from: String, val pos: Pos) {
    R("R", Pos(1, 0)),
    L("L", Pos(-1, 0)),
    U("U", Pos(0, 1)),
    D("D", Pos(0, -1));

    companion object {
        fun createFrom(from: String) =
            Direction.values().firstOrNull { it.from == from } ?: throw Exception("")
    }

}

data class RopeMove(
    val direction: Direction,
    val howMany: Int
)

fun Int.notContainShape(): Int =
    if(this == 0) 0
    else this / abs(this)

data class Pos(val x: Int, val y: Int) {
    val key = "$x:$y"

    fun move(pos: Pos) =
        Pos(x + pos.x, y + pos.y)

    fun track(pos: Pos): Pos =
        if(abs(pos.x - x) <= 1 && abs(pos.y - y) <= 1) {
            this
        } else {
            val offsetX = (pos.x - x).notContainShape()
            val offsetY = (pos.y - y).notContainShape()
            Pos(x + offsetX, y + offsetY)
        }
}