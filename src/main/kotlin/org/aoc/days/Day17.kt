package org.aoc.days

import org.aoc.data.AoC
import org.aoc.toward

class Day17: AoC<List<Day17.WindDirection>, Int, Long>() {

    override fun part1(parsedData: List<WindDirection>): Int {
        var index = 0
        val board = Board()
        var cShape = Shape(Coords(2, 3), Shapes.SHAPE__)
        var stopped = 0
        do {
            val wind = parsedData[index % parsedData.size]
            if(wind == WindDirection.L && !board.hasCollision(cShape.moveLeft())) {
                cShape = cShape.moveLeft()
            } else if(wind == WindDirection.R && !board.hasCollision(cShape.moveRight())) {
                cShape = cShape.moveRight()
            }

            if(!board.hasCollision(cShape.moveBottom())) {
                cShape = cShape.moveBottom()
            } else {
                board.addShape(cShape)
                cShape = cShape.nextShape(board.maxY)
                ++stopped
            }
            index++
        } while(stopped < 2022)
        return board.maxY.toInt()
    }

    override fun part2(parsedData: List<WindDirection>): Long {
        var index = 0
        val board = Board()
        var cShape = Shape(Coords(2, 3), Shapes.SHAPE__)
        var stopped = 0L
        var wind: WindDirection
        val repeats = parsedData.size * Shapes.values().size
        do {
            wind = parsedData[index]
            if(wind == WindDirection.L && !board.hasCollision(cShape.moveLeft())) {
                cShape = cShape.moveLeft()
            } else if(wind == WindDirection.R && !board.hasCollision(cShape.moveRight())) {
                cShape = cShape.moveRight()
            }

            if(!board.hasCollision(cShape.moveBottom())) {
                cShape = cShape.moveBottom()
            } else {
                board.addShape(cShape)
                cShape = cShape.nextShape(board.maxY)
                ++stopped
            }
            index++
            if(index >= parsedData.size) index = 0
        } while(stopped < repeats)
        println(board.maxY)
        println(repeats)
        val units = board.maxY * (2022 / repeats)
        return units
    }

    override fun parseInput(input: String, step: Int): List<WindDirection> {
        val queue = mutableListOf<WindDirection>()
        input.forEach {
            when (it) {
                '<' -> queue.add(WindDirection.L)
                else -> queue.add(WindDirection.R)
            }
        }
        return queue
    }

    data class Shape(
        val leftBottomCoords: Coords,
        val shape: Shapes
    ) {

        fun allCoords(): List<Coords> {
            val m = mutableListOf<Coords>()
            for((yIndex, y) in (shape.positions.size - 1 toward 0).withIndex()) {
                for(x in 0 until shape.positions[y].size) {
                    if(shape.positions[y][x]) {
                        m.add(Coords(leftBottomCoords.x + x, leftBottomCoords.y + yIndex))
                    }
                }
            }
            return m
        }

        fun nextShape(y: Long) = Shape(Coords(2, y + 3L), shape.nextShape())

        fun moveLeft() = Shape(Coords(leftBottomCoords.x - 1, leftBottomCoords.y), shape)
        fun moveRight() = Shape(Coords(leftBottomCoords.x + 1, leftBottomCoords.y), shape)
        fun moveBottom() = Shape(Coords(leftBottomCoords.x, leftBottomCoords.y - 1), shape)

    }
    data class Board(
        val coords: MutableMap<Coords, Boolean> = mutableMapOf()
    ) {
        var maxY: Long = 0

        fun hasCollision(shape: Shape) = shape.allCoords().any { coords[it] == true || it.x > 6 || it.x < 0 || it.y < 0 }

        fun addShape(shape: Shape) {
            for(coord in shape.allCoords()) {
                if(coord.y + 1 > maxY) {
                    maxY = coord.y + 1
                }
                coords[coord] = true
            }
        }
    }

    data class Coords(val x: Long, val y: Long)

    enum class Shapes(
        val positions: List<List<Boolean>>
    ) {
        SHAPE__(listOf(
            listOf(true, true, true, true)
        )),
        SHAPE_PLUS(listOf(
            listOf(false, true, false),
            listOf(true, true, true),
            listOf(false, true, false)
        )),
        SHAPE_L(listOf(
            listOf(false, false, true),
            listOf(false, false, true),
            listOf(true, true, true)
        )),
        SHAPE_I(listOf(
            listOf(true),
            listOf(true),
            listOf(true),
            listOf(true)
        )),
        SHAPE_Q(listOf(
            listOf(true, true),
            listOf(true, true)
        ));

        fun nextShape() = when(this) {
            SHAPE__ -> SHAPE_PLUS
            SHAPE_PLUS -> SHAPE_L
            SHAPE_L -> SHAPE_I
            SHAPE_I -> SHAPE_Q
            SHAPE_Q -> SHAPE__
        }

    }

    enum class WindDirection { L, R }

}
