package org.aoc.days

import org.aoc.data.AoC
import java.util.*

class Day22 : AoC<Day22.Board, Int, Int>() {


    override fun part1(parsedData: Board): Int {
        return parsedData.execute()
    }

    override fun part2(parsedData: Board): Int {
        return 0
    }

    override fun parseInput(input: String, step: Int): Board {
        val tiles = mutableListOf<Tile>()
        val commands = LinkedList<Command>()
        var parsingTiles = true
        val lines = input.lines()
        for (i in 1..lines.size) {
            if (lines[i - 1].isEmpty()) {
                parsingTiles = false
                continue
            }
            if (parsingTiles) {
                lines[i - 1].addTiles(i, tiles)
            } else {
                lines[i - 1].addCommands(commands)
            }
        }
        return Board(tiles, commands)
    }


    private fun String.addCommands(commands: LinkedList<Command>) {
        val characters = toCharArray()
        var numAsString = StringBuilder()
        for (character in characters) {
            if (character == 'L' || character == 'R') {
                if (numAsString.toString().isNotEmpty()) {
                    commands.add(Command.createCommand(numAsString.toString().toInt()))
                    numAsString = StringBuilder()
                }
                commands.add(Command.createCommand(character == 'L'))
            } else {
                numAsString.append(character)
            }
        }
        if (numAsString.toString().isNotEmpty()) {
            commands.add(Command.createCommand(numAsString.toString().toInt()))
        }
    }

    private fun String.addTiles(index: Int, tiles: MutableList<Tile>) {
        val characters = toCharArray()
        for (col in 1..characters.size) {
            val character = characters[col - 1]
            val isWall = character == '#'
            val isGround = character == '.'
            if (isWall || isGround) {
                tiles.add(Tile(col, index, isWall))
            }
        }


    }


    data class Tile(
        val x: Int,
        val y: Int,
        val wall: Boolean
    ) {
        private var leftTile: Tile? = null
        private var rightTile: Tile? = null
        private var upTile: Tile? = null
        private var downTile: Tile? = null

        fun getLeftTile(tiles: List<Tile>): Tile {
            if (leftTile == null) {
                val yTiles = tiles.filter { it.y == this.y }
                val leftTile = yTiles.firstOrNull { it.x == this.x - 1 } ?: yTiles.maxBy { it.x }
                this.leftTile = leftTile
            }
            return leftTile!!
        }

        fun getRightTile(tiles: List<Tile>): Tile {
            if (rightTile == null) {
                val yTiles = tiles.filter { it.y == this.y }
                val rightTile = yTiles.firstOrNull { it.x == this.x + 1 } ?: yTiles.minBy { it.x }
                this.rightTile = rightTile
            }
            return rightTile!!
        }

        fun getUpTile(tiles: List<Tile>): Tile {
            if (upTile == null) {
                val xTiles = tiles.filter { it.x == this.x }
                val upTile = xTiles.firstOrNull { it.y == this.y - 1 } ?: xTiles.maxBy { it.y }
                this.upTile = upTile
            }
            return upTile!!
        }

        fun getDownTile(tiles: List<Tile>): Tile {
            if (downTile == null) {
                val xTiles = tiles.filter { it.x == this.x }
                val downTile = xTiles.firstOrNull { it.y == this.y + 1 } ?: xTiles.minBy { it.y }
                this.downTile = downTile
            }
            return downTile!!
        }


    }

    class RotateCommand(
        private val rotateLeft: Boolean
    ) : Command() {

        override fun execute(board: Board) {
            board.rotate(rotateLeft)
        }

        override fun toString(): String {
            return "RotateCommand(rotateLeft=$rotateLeft)"
        }
    }

    class WalkCommand(
        private val number: Int
    ) : Command() {
        override fun execute(board: Board) {
            board.moveBy(number)
        }

        override fun toString(): String {
            return "WalkCommand(number=$number)"
        }


    }

    abstract class Command {
        companion object {
            fun createCommand(rotateLeft: Boolean) = RotateCommand(rotateLeft)

            fun createCommand(number: Int) = WalkCommand(number)
        }

        abstract fun execute(board: Board)

    }

    enum class Direction(
        val facing: Int,
        val rotate: (rotateLeft: Boolean) -> Direction
    ) {
        RIGHT(0, { rotateLeft -> if (rotateLeft) UP else DOWN }),
        DOWN(1, { rotateLeft -> if (rotateLeft) RIGHT else LEFT }),
        LEFT(2, { rotateLeft -> if (rotateLeft) DOWN else UP }),
        UP(3, { rotateLeft -> if (rotateLeft) LEFT else RIGHT })
    }

    data class Board(
        val tiles: List<Tile>,
        val commands: LinkedList<Command>
    ) {
        private val tilesPositions = tiles.associateBy { it.x to it.y }

        private var currentTile = tiles.filter { it.y == 1 }.minBy { it.x }
        private var direction = Direction.RIGHT

        fun execute(): Int {
            do {
                val command = commands.poll()
                command.execute(this)
            } while (commands.isNotEmpty())
            return 1000 * currentTile.y + 4 * currentTile.x + direction.facing
        }

        fun moveBy(number: Int) {
            for (i in 0 until number) {
                val nextTile = when (direction) {
                    Direction.RIGHT -> currentTile.getRightTile(tiles)
                    Direction.DOWN -> currentTile.getDownTile(tiles)
                    Direction.LEFT -> currentTile.getLeftTile(tiles)
                    Direction.UP -> currentTile.getUpTile(tiles)
                }
                if (nextTile.wall) break
                currentTile = nextTile
            }
        }

        fun rotate(rotateLeft: Boolean) {
            direction = direction.rotate(rotateLeft)
        }

        fun printTiles() {
            val minX = 1
            val maxX = tiles.maxOf { it.x }
            val minY = 1
            val maxY = tiles.maxOf { it.y }
            for (y in minY..maxY) {
                for (x in minX..maxX) {
                    val tile = tilesPositions[x to y]
                    val s = when {
                        tile == null -> ' '
                        tile.wall -> '#'
                        else -> '.'
                    }
                    print(s)
                }
                println()
            }
        }

    }
}
