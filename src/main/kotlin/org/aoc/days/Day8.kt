package org.aoc.days

import org.aoc.data.AoC

class Day8 : AoC<List<List<Int>>, Int, Int>() {

    override fun part1(parsedData: List<List<Int>>): Int {
        var visibleTrees = 0
        parsedData.forEachIndexed { rowIdx, treeRow ->
            treeRow.forEachIndexed { colIdx, treeCol ->
                if (rowIdx == 0 || rowIdx == parsedData.size - 1
                    || colIdx == 0 || colIdx == treeRow.size - 1
                ) {
                    visibleTrees++
                } else {
                    val c1 = { (0 until colIdx).all { treeRow[it] < treeCol } }
                    val c2 = { (colIdx + 1 until treeRow.size).all { treeRow[it] < treeCol } }
                    val r1 = { (0 until rowIdx).all { parsedData[it][colIdx] < treeCol } }
                    val r2 = { (rowIdx + 1 until parsedData.size).all { parsedData[it][colIdx] < treeCol } }
                    if (c1() || c2() || r1() || r2()) visibleTrees++
                }
            }
        }
        return visibleTrees
    }

    override fun part2(parsedData: List<List<Int>>): Int {
        val viewScores = mutableListOf<Int>()
        parsedData.forEachIndexed { rowIdx, treeRow ->
            treeRow.forEachIndexed { colIdx, treeCol ->
                var left = 0
                for (idx in colIdx - 1 downTo 0) {
                    if (treeRow[idx] < treeCol) left++
                    else {
                        left++; break
                    }
                }
                var right = 0
                for (idx in colIdx + 1..(treeRow.size - 1)) {
                    if (treeRow[idx] < treeCol) right++
                    else {
                        right++; break
                    }
                }
                var top = 0
                for (idx in rowIdx - 1 downTo 0) {
                    if (parsedData[idx][colIdx] < treeCol) top++
                    else {
                        top++; break
                    }
                }
                var bottom = 0
                for (idx in rowIdx + 1..parsedData.size - 1) {
                    if (parsedData[idx][colIdx] < treeCol) bottom++
                    else {
                        bottom++; break
                    }
                }
                viewScores.add(bottom * top * left * right)
            }
        }
        return viewScores.max()
    }

    override fun parseInput(input: String, step: Int): List<List<Int>> =
        input
            .split("\n")
            .map { line ->
                line
                    .map { it.digitToInt() }
            }

}