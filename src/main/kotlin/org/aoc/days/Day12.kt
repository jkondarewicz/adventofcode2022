package org.aoc.days

import org.aoc.data.AoC

class Day12: AoC<List<List<Graph>>, Int, Int>() {
    override fun part1(parsedData: List<List<Graph>>): Int {
        val start = parsedData.flatten().first { it.start }
        return dijkstra(parsedData, start)
            .values
            .first { it.value == 'E' }
            .distance
    }

    override fun part2(parsedData: List<List<Graph>>): Int {
        val end = parsedData.flatten().first { it.end }
        return dijkstra(parsedData, end, reversed = true)
            .values
            .filter { it.value == 'a' || it.value == 'S' }
            .minOf { it.distance }
    }

    override fun parseInput(input: String, step: Int): List<List<Graph>> {
        val fGraph = mutableListOf<MutableList<Graph>>();
        input.split("\n").let { lines ->
            lines.flatMapIndexed { row, line ->
                fGraph.add(mutableListOf())
                line.toCharArray().mapIndexed { col, character ->
                    fGraph[row].add(Graph(row, col, character))
                }
            }
        }
        fGraph.forEachIndexed { row, listOfRowGraphs ->
            listOfRowGraphs.forEachIndexed { col, graph ->
                graph
                    .addConnection(fGraph.getValueAt(row - 1, col))
                    .addConnection(fGraph.getValueAt(row + 1, col))
                    .addConnection(fGraph.getValueAt(row, col + 1))
                    .addConnection(fGraph.getValueAt(row, col - 1))
            }
        }
        return fGraph
    }
}


data class Graph(
    val y: Int,
    val x: Int,
    val value: Char,
) {
    private val weight: Int = value.normalized.code
    val connections: MutableList<Graph> = mutableListOf()
    val rConnections: MutableList<Graph> = mutableListOf()

    val start = value == 'S'
    val end = value == 'E'

    fun addConnection(graph: Graph?): Graph {
        if(graph != null && graph.weight - weight <= 1) {
            connections.add(graph)
        }
        if(graph != null && graph.weight - weight >= -1) {
            rConnections.add(graph)
        }
        return this
    }

    private val Char.normalized: Char
        get() = when(this) {
            'S' -> 'a'
            'E' -> 'z'
            else -> this
        }
}

fun<T> List<List<T>>.getValueAt(row: Int, col: Int): T? {
    if(row >= 0 && row < this.size) {
        if(col >= 0 && col < this[row].size) {
            return this[row][col]
        }
    }
    return null
}

data class GraphLocation(val y: Int, val x: Int) {
    constructor(g: Graph): this(g.y, g.x)
}

data class VertexDistance(
    val value: Char,
    val distance: Int
) {
    constructor(g: Graph, distance: Int): this(g.value, distance)
}

fun dijkstra(graph: List<List<Graph>>, source: Graph, reversed: Boolean = false): MutableMap<GraphLocation, VertexDistance> {
    val distance = mutableMapOf<GraphLocation, VertexDistance>()
    val Q = mutableMapOf<GraphLocation, Graph>()
    for(y in graph.indices) {
        for(x in 0 until graph[y].size) {
            val v = graph[y][x]
            distance[GraphLocation(v)] = VertexDistance(v, Int.MAX_VALUE)
            Q[GraphLocation(v)] = v
        }
    }
    distance[GraphLocation(source)] = VertexDistance(source, 0)
    while(Q.isNotEmpty()) {
        findMinDistance(distance, Q)?.let { key ->
            val v: Graph = Q.remove(key)!!
            when {
                reversed -> v.rConnections
                else -> v.connections
            }.forEach {
                if(Q.containsKey(GraphLocation(it))) {
                    val alt = distance[key]!!.distance + 1
                    if(alt < distance[GraphLocation(it)]!!.distance) {
                        distance[GraphLocation(it)] = VertexDistance(it, alt)
                    }
                }
            }
        } ?: break
    }
    return distance
}

fun findMinDistance(distance: MutableMap<GraphLocation, VertexDistance>, Q: MutableMap<GraphLocation, Graph>): GraphLocation? {
    var key: GraphLocation? = null
    var minDistance: Int = Int.MAX_VALUE
    distance.forEach { (k, value) ->
        if(value.distance < minDistance && Q.containsKey(k)) {
            minDistance = value.distance
            key = k
        }
    }
    return key
}

