package org.aoc.days

import org.aoc.data.AoC

class Day1: AoC<List<Day1.ElfFood>, Int, Int>() {
    override fun part1(parsedData: List<ElfFood>): Int {
        return parsedData.map { it.all }.sortedDescending()[0]
    }

    override fun part2(parsedData: List<ElfFood>): Int {
        return parsedData.map { it.all }.sortedDescending().subList(0, 3).sum()
    }

    override fun parseInput(input: String, step: Int): List<ElfFood> {
        val elfFoods = mutableListOf<ElfFood>()
        var singleElfFoodQuantity = mutableListOf<Int>()
        input.lines().forEach {
            if(it.isNotEmpty()) {
                singleElfFoodQuantity.add(it.toInt())
            } else {
                elfFoods.add(ElfFood(singleElfFoodQuantity))
                singleElfFoodQuantity = mutableListOf()
            }
        }
        if(singleElfFoodQuantity.isNotEmpty()) {
            elfFoods.add(ElfFood(singleElfFoodQuantity))
        }
        return elfFoods
    }

    data class ElfFood(
        val food: List<Int>
    ) {
        val all = food.sum()
    }

}