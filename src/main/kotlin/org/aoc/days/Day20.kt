package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.abs

class Day20: AoC<List<Day20.NumberConnect>, Int, Long>() {


    override fun part1(parsedData: List<NumberConnect>): Int {
        parsedData.mix()
        val idx = parsedData.toIndexedArray()
        return (1000..3000 step 1000)
            .sumOf { idx[it % parsedData.size].toInt() }
    }

    override fun part2(parsedData: List<NumberConnect>): Long {
        parsedData.mix(10)
        val idx = parsedData.toIndexedArray()
        return (1000..3000 step 1000)
            .sumOf { idx[it % parsedData.size] }
    }

    override fun parseInput(input: String, step: Int): List<NumberConnect> =
        input.lines().let { lines ->
            val multiplier = if(step == 1) 1 else 811589153
            val l = mutableListOf(NumberConnect(multiplier * lines[0].toLong()))
            (1 until lines.size).forEach {
                val current = NumberConnect(multiplier * lines[it].toLong())
                current.setPreviousNumber(l[it - 1])
                l[it - 1].setNextNumber(current)
                l.add(current)
            }
            l[0].setPreviousNumber(l[l.size - 1])
            l
        }

    private fun chooseWay(num: Long, size: Int): Long {
        if(num == 0L)
            return 0L
        val offset = num % (size - 1)
        val absOffset = abs(offset)
        val reversedOffset = size - absOffset
        val absReversedOffset = abs(reversedOffset)
        return if(absOffset < absReversedOffset) offset
        else ((-(offset / absOffset) * reversedOffset).let { if(it < 0) it + 1 else it - 1 })
    }

    private fun List<NumberConnect>.mix(times: Int = 1) {
        for (i in 0 until times) {
            forEach { numberConnections ->
                val loop = chooseWay(numberConnections.number, size)
                var n = numberConnections
                for (i in 0 until abs(loop)) {
                    if (loop > 0) n = n.nextNumber
                    else if (loop < 0) n = n.previousNumber
                }
                if (loop > 0) {
                    numberConnections.nextNumber.setPreviousNumber(numberConnections.previousNumber)
                    n.nextNumber.setPreviousNumber(numberConnections)
                    n.setNextNumber(numberConnections)
                } else if (loop < 0) {
                    numberConnections.previousNumber.setNextNumber(numberConnections.nextNumber)
                    n.previousNumber.setNextNumber(numberConnections)
                    n.setPreviousNumber(numberConnections)
                }
            }
        }
    }


    private fun List<NumberConnect>.toIndexedArray() =
        this.first { it.number == 0L }.let {
            val l = mutableListOf<Long>()
            var n = it
            do {
                l.add(n.number)
                n = n.nextNumber
            } while(n.number != 0L)
            l
        }

    data class NumberConnect(
        val number: Long
    ) {
        lateinit var nextNumber: NumberConnect
        lateinit var previousNumber: NumberConnect

        fun setNextNumber(number: NumberConnect): NumberConnect {
            this.nextNumber = number
            number.previousNumber = this
            return this
        }

        fun setPreviousNumber(number: NumberConnect): NumberConnect {
            this.previousNumber = number
            number.nextNumber = this
            return this
        }
    }

}