package org.aoc.days

import org.aoc.data.AoC

class Day21: AoC<List<Day21.Monkey>, Long, Long>()  {
    override fun part1(parsedData: List<Monkey>): Long {
        return parsedData.first { it.name == "root" }.getNumber(parsedData)!!
    }

    override fun part2(parsedData: List<Monkey>): Long {
        parsedData.first { it.name == "humn" }.number = null
        val root = parsedData.first { it.name == "root" }
        val firstMonkey = parsedData.first { it.name == root.firstMonkeyName }
        val secondMonkey = parsedData.first { it.name == root.secondMonkeyName }
        val fv = firstMonkey.getNumber(parsedData)
        val sv = secondMonkey.getNumber(parsedData)
        return if(fv == null) firstMonkey.findHumnValue(parsedData, sv!!)
        else secondMonkey.findHumnValue(parsedData, fv)
    }

    override fun parseInput(input: String, step: Int): List<Monkey> =
        input.lines().map {
            it.filter { !it.isWhitespace() }.let { line ->
                line.split(":").let { data ->
                    if(Operation.values().map { it.from }.any { data[1].contains(it) }) {
                        val monkey1 = data[1].substring(0, 4)
                        val monkey2 = data[1].substring(5, 9)
                        val operation = data[1].substring(4, 5).let { Operation.from(it) }
                        Monkey(data[0], monkey1, monkey2, operation)
                    } else {
                        Monkey(data[0], data[1].toLong())
                    }
                }
            }
        }

    data class Monkey(
        val name: String,
        var number: Long?
    ) {

        var firstMonkeyName: String? = null
        var secondMonkeyName: String? = null
        var operation: Operation? = null
        constructor(name: String, firstMonkeyName: String, secondMonkeyName: String, operation: Operation): this(name, null) {
            this.firstMonkeyName = firstMonkeyName
            this.secondMonkeyName = secondMonkeyName
            this.operation = operation
        }

        fun isHumn() = name == "humn"

        fun findHumnValue(monkeys: List<Monkey>, value: Long): Long {
            if(isHumn()) return value
            val first = monkeys.first { it.name == firstMonkeyName }
            val second = monkeys.first { it.name == secondMonkeyName }
            val fv = first.getNumber(monkeys)
            val sv = second.getNumber(monkeys)
            return if(fv == null) {
                first.findHumnValue(monkeys, operation!!.opposite().operation(value, sv!!))
            } else {
                val op = operation!!
                if(op == Operation.DIVIDE || op == Operation.MINUS)
                    second.findHumnValue(monkeys, op.operation(fv, value))
                else
                    second.findHumnValue(monkeys, op.opposite().operation(value, fv))
            }
        }

        fun getNumber(monkeys: List<Monkey>): Long? {
            val n = number
            if(n != null) return n
            if(firstMonkeyName == null) return null
            val firstMonkey = monkeys.first { it.name == firstMonkeyName }.getNumber(monkeys)
            val secondMonkey = monkeys.first { it.name == secondMonkeyName }.getNumber(monkeys)
            if(firstMonkey == null || secondMonkey == null) return null
            val result = operation!!.operation(firstMonkey, secondMonkey)
            number = result
            return number!!
        }

        override fun toString(): String {
            return "Monkey(name='$name', number=$number, firstMonkeyName=$firstMonkeyName, secondMonkeyName=$secondMonkeyName, operation=$operation)"
        }


    }

    enum class Operation(
        val from: String,
        val operation: (Long, Long) -> Long,
        val opposite: () -> Operation
    ) {
        ADD("+", { a, b -> a + b }, { org.aoc.days.Day21.Operation.MINUS }),
        MULTIPLY("*", { a, b -> a * b}, { org.aoc.days.Day21.Operation.DIVIDE }),
        DIVIDE("/", { a, b -> a / b }, { org.aoc.days.Day21.Operation.MULTIPLY }),
        MINUS("-", { a, b -> a - b}, { org.aoc.days.Day21.Operation.ADD });

        companion object {
            fun from(str: String) =
                Operation.values().first { it.from == str }
        }

    }

}