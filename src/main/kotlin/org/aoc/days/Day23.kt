package org.aoc.days

import org.aoc.data.AoC

class Day23: AoC<Day23.Board, Int, Int>()  {
    override fun part1(parsedData: Board): Int {
        (0 until 10).forEach { _ -> parsedData.round() }
        return (parsedData.maxX() - parsedData.minX() + 1) * (parsedData.maxY() - parsedData.minY() + 1) - parsedData.elfs.size
    }

    override fun part2(parsedData: Board): Int {
        var round = 0
        do {
            parsedData.round()
            round++
        } while(!parsedData.elfs.none { it.consideringMove })
        return round
    }

    override fun parseInput(input: String, step: Int): Board =
        input.lines().let { lines ->
            val size = lines.size
            lines.mapIndexedNotNull { y, line ->
                line.toCharArray().toList().mapIndexedNotNull { x, ground ->
                    if (ground == '#')
                        Elf(Cords(x, size - y))
                    else null
                }
            }
                .flatten()
                .let { Board(it) }
        }
    data class Cords(
        val x: Int,
        val y: Int
    ) {
        fun up() = Cords(x, y + 1)
        fun down() = Cords(x, y - 1)
        fun left() = Cords(x - 1, y)
        fun right() = Cords(x + 1, y)
    }

    enum class Direction(
        val move: (sourceCords: Cords) -> Cords,
        val opposite: () -> Direction,
        val nextDirection: () -> Direction,
        val placesToLook: (sourceCords: Cords) -> List<Cords>
    ) {
        N({ it.up() }, {S}, {S}, { cords -> listOf(cords.up().left(), cords.up(), cords.up().right()) }),
        S({ it.down() }, {N}, {W}, { cords -> listOf(cords.down().left(), cords.down(), cords.down().right()) }),
        W({ it.left() }, {E}, {E}, { cords -> listOf(cords.down().left(), cords.left(), cords.up().left()) }),
        E({ it.right() }, {W}, {N}, { cords -> listOf(cords.down().right(), cords.right(), cords.up().right()) });

        companion object {
            fun allPlacesToLook(sourceCords: Cords): Set<Cords> =
                listOf(
                    N.placesToLook(sourceCords),
                    S.placesToLook(sourceCords),
                    E.placesToLook(sourceCords),
                    W.placesToLook(sourceCords)
                )
                    .flatten()
                    .toSet()
        }
    }

    data class Elf(
        var cords: Cords,
        var consideringDirection: Direction = Direction.N,
        var consideringMove: Boolean = true
    ) {
        fun nextDirection() {
            consideringDirection = consideringDirection.nextDirection()
        }
        fun moveIfPossible(possible: Boolean) {
            if(possible)
                cords = consideringDirection.move(cords)
        }
    }

    data class Board(
        val elfs: List<Elf>
    ) {

        var direction = Direction.N
        fun round(): Board {
            val elfPositions = elfs.associateBy { it.cords }.toMutableMap()
            for(elf in elfs) {
                elf.consideringDirection = direction
                elf.consideringMove = true
                if(Direction.allPlacesToLook(elf.cords).all { !elfPositions.containsKey(it) }) {
                    elf.moveIfPossible(false)
                    elf.consideringMove = false
                    continue
                }
                var canMove = false
                for(i in 0..3) {
                    canMove = !elf.consideringDirection.placesToLook(elf.cords).any { elfPositions.containsKey(it) }
                    if(!canMove)
                        elf.nextDirection()
                    else
                        break
                }
                elf.consideringMove = canMove
                if(!canMove) {
                    elf.moveIfPossible(false)
                }
            }
            for(elf in elfs.filter { it.consideringMove }) {
                val elfTwoTiles = elfPositions[elf.consideringDirection.move(elf.consideringDirection.move(elf.cords))]
                val cannotMove = elfTwoTiles?.consideringMove == true && elfTwoTiles.consideringDirection == elf.consideringDirection.opposite()
                elf.moveIfPossible(!cannotMove)
            }
            direction = direction.nextDirection()
            return this
        }

        fun minX() = elfs.minOf { it.cords.x }
        fun maxX() = elfs.maxOf { it.cords.x }
        fun minY() = elfs.minOf { it.cords.y }
        fun maxY() = elfs.maxOf { it.cords.y }

    }

}