package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.max

class Day16: AoC<List<Day16.Valve>, Int, Int>() {
    override fun part1(parsedData: List<Valve>): Int {
        val limit = 30
        var cPath = Path(0, parsedData.first { it.name == "AA" })
        var steps = 0
        var accumulative = 0
        do {
            val paths = cPath.all()
            if(paths.isNotEmpty()) {
                val nextPath = paths.fold(paths[0]) { acc, path ->
                    val idx = max(acc.steps, path.steps) + 1
                    if(acc.calculateValueFor(idx) > path.calculateValueFor(idx)) acc
                    else path
                }
                cPath = Path(0, nextPath.valve)
                steps += nextPath.steps + 1
                if(steps > limit) break
                accumulative += (limit - steps) * nextPath.valve.getValue()
                nextPath.valve.open()
            } else {
                println("sss")
                break
            }
        } while(steps < limit)
        return accumulative
    }

    override fun part2(parsedData: List<Valve>): Int {
        val limit = 26
        var cPath = Path(0, parsedData.first { it.name == "AA" })
        var ePath = Path(0, parsedData.first { it.name == "AA" })
        var cSteps = 0
        var eSteps = 0
        var accumulative = 0
        do {
            val paths = cPath.all()
            if(cSteps <= eSteps && paths.isNotEmpty()) {
                val nextPath = paths.fold(paths[0]) { acc, path ->
                    val idx = max(acc.steps, path.steps) + 1
                    if(acc.calculateValueFor(idx) > path.calculateValueFor(idx)) acc
                    else path
                }
                cPath = Path(0, nextPath.valve)
                cSteps += nextPath.steps + 1
                if(cSteps < limit) {
                    println("c: $nextPath")
                    accumulative += (limit - cSteps) * nextPath.valve.getValue()
                    nextPath.valve.open()
                }
            }
            val ePaths = ePath.all()
            if(eSteps <= cSteps && ePaths.isNotEmpty()) {
                val nextPath = ePaths.fold(ePaths[0]) { acc, path ->
                    val idx = max(acc.steps, path.steps) + 1
                    if(acc.calculateValueFor(idx) > path.calculateValueFor(idx)) acc
                    else path
                }
                ePath = Path(0, nextPath.valve)
                eSteps += nextPath.steps + 1
                if(eSteps < limit) {
                    println("e: $nextPath")
                    accumulative += (limit - eSteps) * nextPath.valve.getValue()
                    nextPath.valve.open()
                }
            }
            if(ePaths.isEmpty() && paths.isEmpty()) {
                break
            }
        } while(cSteps < limit && eSteps < limit)
        return accumulative
    }

    override fun parseInput(input: String, step: Int): List<Valve> =
        input
            .lines()
            .map { line ->
                val name = line.substring(6, 8)
                val flowRate = line.substring(23, line.length).split(";")[0].toInt()
                val connections = line.split(";")[1].let { it.substring(it.getIndex(), it.length) }.split(", ")
                Valve(name, flowRate) to connections
            }
            .let { valveWithConnections ->
                valveWithConnections.map {
                    val valve = it.first
                    val connections = it.second
                    valve.apply {
                        connections.forEach { valveName ->
                            this.connections.add(valveWithConnections.first { it.first.name == valveName}.first)
                        }
                    }
                }
            }

    private fun String.getIndex() =
        if(contains("tunnels")) 24
        else 23

    data class Path(val steps: Int, val valve: Valve) {

        fun calculateValueFor(step: Int) =
            (step - steps) * valve.getValue()

        fun all(): List<Path> =
            findAll()
                .groupBy { it.valve }
                .map { it.value.minBy { it.steps } }

        private fun findAll(visited: MutableMap<Valve, Int> = mutableMapOf()): List<Path> {
            visited[this.valve] = steps
            val paths = mutableListOf<Path>()
            if(this.valve.getValue() > 0) {
                paths.add(this)
            }
            valve.connections.forEach {
                if(!visited.containsKey(it) || visited[it]!! > steps + 1) {
                    paths.addAll(Path(steps + 1, it).findAll(visited))
                }
            }
            return paths
        }

        override fun toString(): String {
            return "Path(steps=$steps, valve=$valve)"
        }
    }

    data class Valve(
        val name: String,
        val flowRate: Int,
    ) {
        val connections: MutableList<Valve> = mutableListOf()

        private var valveOpened = false

        fun getValue() = if(valveOpened) 0 else flowRate

        fun open() {
            valveOpened = true
        }

        override fun toString(): String {
            return "Valve(name=$name, flowRate=$flowRate, connections=[${connections.joinToString(", ") { it.name }}]"
        }
    }
}