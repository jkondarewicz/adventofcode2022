package org.aoc.days

import org.aoc.data.AoC

class Day7: AoC<DirNode, Int, Int>() {

    override fun part1(parsedData: DirNode): Int {
        val top = parsedData.toTop()
        return top
            .filter { it.getTotalSize() <= 100000 }
            .sumOf { it.getTotalSize() }
    }

    override fun part2(parsedData: DirNode): Int {
        val allSize = parsedData.toTop().getTotalSize()
        val minSpaceToFree = 30000000 - (70000000 - allSize)
        return parsedData.toTop()
            .filter { true }
            .map { it.getTotalSize() }
            .sorted()
            .first { it > minSpaceToFree }
    }

    override fun parseInput(input: String, step: Int): DirNode {
        var dirNode = DirNode("/", null)
        input.split("\n").forEach { line ->
            if(line.startsWith("$ cd")) {
                val dir = line.split(" ")[2]
                dirNode = when(dir) {
                    ".." -> dirNode.toParent()
                    "/" -> dirNode.toTop()
                    else -> dirNode.toChildren(dir)
                }
            } else if( !line.startsWith("$") && !line.startsWith("dir") ) {
                val d = line.split(" ")
                dirNode.addFile(d[1], d[0].toInt())
            }
        }
        return dirNode.toTop()
    }

}

data class DirNode(
    val path: String,
    val parent: DirNode?
) {
    val files = mutableMapOf<String, Int>()
    val children = mutableListOf<DirNode>()

    fun getTotalSize(): Int = files.values.fold(0) { acc, value -> acc + value } + children.map { it.getTotalSize() }.sum()

    fun addFile(name: String, size: Int) {
        files[name] = size
    }

    fun toTop(): DirNode =
        parent?.let { it.toTop() } ?: this

    fun toParent(): DirNode =
        parent ?: this

    fun toChildren(lastPath: String): DirNode {
        val p = "$path$lastPath"
        return children
            .firstOrNull { it.path == p }
            ?: DirNode(p, this).also(children::add)
    }

    fun filter(condition: (DirNode) -> Boolean): List<DirNode> {
        val l = mutableListOf<DirNode>()
        if(condition(this)) l.add(this)
        children.forEach {
            l.addAll(it.filter(condition))
        }
        return l
    }

}