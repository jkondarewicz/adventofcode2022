package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.pow

class Day25: AoC<List<Day25.NumberSystem>, String, Int>()  {
    override fun part1(parsedData: List<NumberSystem>): String {
        return parsedData
            .fold(0) { acc, value -> acc + value.decimal }
            .let(NumberSystem::fromDecimal)
            .snafu
    }

    override fun part2(parsedData: List<NumberSystem>): Int {
        return 0
    }

    override fun parseInput(input: String, step: Int): List<NumberSystem> =
        input.lines().map { NumberSystem.fromSnafu(it) }

    data class NumberSystem private constructor(
        val snafu: String,
        val decimal: Int
    ) {
        companion object {
            fun fromSnafu(snafu: String): NumberSystem {
                val decimal = snafu.foldIndexed(0) { idx, acc, number ->
                    val nth = snafu.length - idx - 1
                    val num = when (number) {
                        '2' -> 2
                        '1' -> 1
                        '0' -> 0
                        '-' -> -1
                        '=' -> -2
                        else -> 0
                    }
                    acc + 5.0.pow(nth).toInt() * num
                }
                return NumberSystem(snafu, decimal)
            }

            fun fromDecimal(decimal: Int): NumberSystem {
                var nth = -1
                while(5.0.pow(++nth) * 2 < decimal) {}
                val snafu = mutableListOf<Int>()
                var result = 0
                while(nth-- > 0) {
                    for(i in 2 downTo -2) {
                        val value = (5.0.pow(nth) * i).toInt()
                        if(result + value <= decimal) {
                            result += value
                            snafu.add(i)
                            break
                        }
                    }
                }
                println(snafu)
                return NumberSystem("snafu", decimal)
            }
        }
    }

}