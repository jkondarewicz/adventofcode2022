package org.aoc.days

import org.aoc.data.AoC
import org.aoc.toward

class Day14: AoC<Cave, Int, Int>() {
    override fun part1(parsedData: Cave): Int {
        parsedData.clear().let {
            while(it.addSand()) {}
        }
        return parsedData.unit
    }

    override fun part2(parsedData: Cave): Int {
        parsedData.clear().let {
            while(it.addSand(secondStage = true)) {}
        }
        return parsedData.unit
    }

    override fun parseInput(input: String, step: Int): Cave {
        val rocks = mutableSetOf<Position>()
        input.lines().forEach { line ->
            val positions = line
                .split(" -> ")
                .map {
                    it.split(",").let(::Position)
                }
            for(i in 1 until positions.size) {
                rocks.addAll(findLinePositions(positions[i - 1], positions[i]))
            }
        }
        return Cave(rocks)
    }

    private fun findLinePositions(first: Position, second: Position): Set<Position> {
        val pos = mutableSetOf(first)
        for (i in first.x toward second.x ) {
            pos.add(Position(i, first.y))
        }
        for (i in first.y toward second.y ) {
            pos.add(Position(first.x, i))
        }
        pos.add(second)
        return pos
    }

}

data class Cave(
    val rocks: Set<Position>
) {
    var unit: Int = 0
    private val settled = mutableMapOf<Position, Boolean>()

    private val lowestY = rocks.maxOf { it.y }

    fun addSand(secondStage: Boolean = false): Boolean {
        val low = if(secondStage) lowestY + 1 else lowestY
        var sand = Position(500, 0)
        do {
            val canGoB = !settled.containsKey(sand.bottom())
            val canGoLB = !settled.containsKey(sand.left().bottom())
            val canGoRB = !settled.containsKey(sand.right().bottom())
            val additional = (!secondStage) || (sand.bottom().y <= low)
            if(canGoB && additional) sand = sand.bottom()
            else if(canGoLB && additional) sand = sand.left().bottom()
            else if (canGoRB && additional) sand = sand.right().bottom()
            else {
                ++unit
                if(sand == Position(500, 0)) {
                    return false
                }
                settled[sand] = true
                return true
            }
        } while (sand.y < low || secondStage)
        return false
    }

    fun clear(): Cave {
        unit = 0
        settled.clear()
        settled.putAll(
            rocks.map { it to true }
        )
        return this
    }
}
data class Position(val x: Int, val y: Int) {
    constructor(positions: List<String>): this(positions[0].toInt(), positions[1].toInt())

    fun bottom() = Position(x, y + 1)
    fun left() = Position(x - 1, y)
    fun right() = Position(x + 1, y)

}
