package org.aoc.days

import org.aoc.data.AoC

class Day6: AoC<String, Int, Int>() {

    override fun part1(parsedData: String): Int = parsedData.getDistinctCharIndex(4)

    override fun part2(parsedData: String): Int = parsedData.getDistinctCharIndex(14)

    override fun parseInput(input: String, step: Int): String = input

    private fun String.getDistinctCharIndex(howMany: Int): Int {
        for (i in howMany until this.length) {
            val str = this.substring(i - howMany, i).toCharArray().toSet()
            if(str.size == howMany) {
                return i
            }
        }
        return -1
    }

}