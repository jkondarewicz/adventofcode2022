package org.aoc.days

import org.aoc.data.AoC

class Day10: AoC<String, Int, String>() {
    override fun part1(parsedData: String): Int {
        val arr = parsedData.lines().flatMap {
            if(it.startsWith("noop")) listOf(0)
            else listOf(it.split(" ")[1].toInt(), 0)
        }
        return (20 .. 220 step 40).map { arr.sumOfCycles(it) * it }.sum()
    }

    override fun part2(parsedData: String): String {
        var sprite = Sprite(1)
        return parsedData.lines().flatMap {
            if(it.startsWith("noop")) listOf(0)
            else listOf(0, it.split(" ")[1].toInt())
        }.mapIndexed { index, value ->
            (if(sprite.shouldRender(index)) {
                "🎁"
            } else {
                "🎄"
            }).let {
                sprite = sprite.changePosition(value)
                if(index % 40 == 0) "\n$it"
                else it
            }
        }.joinToString("")
    }

    override fun parseInput(input: String, step: Int): String = input
}


fun List<Int>.sumOfCycles(cycles: Int): Int {
    return 1 + this.subList(0, cycles - 2).let { it.sum() }
}

data class Sprite(val startIndex: Int) {

    fun changePosition(offset: Int) = Sprite(startIndex + offset)

    fun shouldRender(index: Int): Boolean {
        return (index % 40).let { it + 1 }.let { it == startIndex || it == startIndex + 1 || it == startIndex + 2 }
    }
}
