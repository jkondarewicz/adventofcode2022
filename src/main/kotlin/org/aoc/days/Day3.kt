package org.aoc.days

import org.aoc.data.AoC

class Day3: AoC<String, Int, Int>() {

    override fun part1(parsedData: String): Int =
        parsedData.lines().fold(0) { acc, value ->
            val x = value.substring(0, value.length / 2)
            val y = value.substring(value.length / 2, value.length)
            val sack1 = x.toCharArray().toHashSet()
            val sack2 = y.toCharArray().toHashSet()
            val common = sack1.intersect(sack2)
            acc + common.sumOf { letter ->
                letter.charValue
            }
        }


    override fun part2(parsedData: String): Int {
        var score = 0
        var bags = mutableListOf<String>()
        var index = 1
        parsedData.lines().forEach { e ->
            bags.add(e)
            if (index % 3 == 0) {
                val sack1 = bags[0].toCharArray().toHashSet()
                val sack2 = bags[1].toCharArray().toHashSet()
                val sack3 = bags[2].toCharArray().toHashSet()
                val common = sack1.intersect(sack2.intersect(sack3))
                common.forEach { letter ->
                    score += letter.charValue
                }
                bags = mutableListOf()
            }
            ++index
        }
        return score
    }

    override fun parseInput(input: String, step: Int): String = input

    private val Char.charValue: Int
        get() = if(isUpperCase()) code - 38 else code - 96

}