package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.abs

class Day15: AoC<Day15.SensorBeaconsMap, Int, Long>() {

    override fun part1(parsedData: SensorBeaconsMap): Int {
        return parsedData.countBeacons(2000000)
    }

    override fun part2(parsedData: SensorBeaconsMap): Long {
        val position = parsedData.findDistressedBeacon()
        return position.x * 4000000L + position.y
    }

    override fun parseInput(input: String, step: Int): SensorBeaconsMap {
        val regex = "x=-?\\d+, y=-?\\d+"
        return input
            .lines()
            .map {
                regex
                    .toPattern()
                    .matcher(it)
                    .results()
                    .map { matchResult -> matchResult.group() }
                    .toList()
                    .let {
                        val sensorPos = it[0].replace("x=", "").replace(" y=", "").split(",")
                        val beaconPos = it[1].replace("x=", "").replace(" y=", "").split(",")
                        val sensor = Position(sensorPos[0].toInt(), sensorPos[1].toInt())
                        val beacon = Position(beaconPos[0].toInt(), beaconPos[1].toInt())
                        SensorBeacon(sensor, beacon)
                    }
            }
            .toMutableList()
            .let(::SensorBeaconsMap)
    }

    data class Position(
        val x: Int,
        val y: Int
    ) {
        fun calculateDistance(pos: Position) =
            abs(x - pos.x) + abs(y - pos.y)
    }

    data class SensorBeacon(
        val sensor: Position,
        val beacon: Position
    ) {
        private val distance = sensor.calculateDistance(beacon)

        fun addIfAffected(line: Int, list: MutableSet<Position>) {
            if(lineAffected(line)) {
                val yDistanceFromSensor = Position(sensor.x, line).calculateDistance(sensor)
                for(i in sensor.x - distance + yDistanceFromSensor .. sensor.x + distance - yDistanceFromSensor) {
                    val pos = Position(i, line)
                    if(pos.calculateDistance(sensor) <= distance) {
                        list.add(pos)
                    }
                }
            }
        }

        fun nextBeaconPossible(pos: Position): Position =
            Position(x = 1 + sensor.x + (distance - abs(pos.y - sensor.y)), y = pos.y)

        fun positionCouldNotContainBeacon(pos: Position) =
            pos.calculateDistance(sensor) <= distance

        private fun lineAffected(line: Int): Boolean {
            return line >= sensor.y - distance && line <= sensor.y + distance
        }

    }

    data class SensorBeaconsMap(
        val sensorsBeacons: MutableList<SensorBeacon> = mutableListOf()
    ) {
        fun countBeacons(line: Int): Int {
            val beaconNotPossible = mutableSetOf<Position>()
            sensorsBeacons
                .forEach { it.addIfAffected(line, beaconNotPossible) }
            sensorsBeacons.forEach { beaconNotPossible.remove(it.beacon) }
            return beaconNotPossible.size
        }

        fun findDistressedBeacon(): Position {
            var position = Position(0, 0)
            do {
                val sensorBeacon = sensorsBeacons.firstOrNull { it.positionCouldNotContainBeacon(position) } ?: break
                position = sensorBeacon.nextBeaconPossible(position)
                if(position.x > 4000000) position = Position(0, position.y + 1)
            } while(true)
            return position
        }
    }
}

