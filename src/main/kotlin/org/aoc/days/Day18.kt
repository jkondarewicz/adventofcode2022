package org.aoc.days

import org.aoc.data.AoC
import kotlin.math.pow
import kotlin.math.sqrt

class Day18: AoC<List<Day18.Coordinates>, Int, Int>() {
    override fun part1(parsedData: List<Coordinates>): Int {
        for(i in 0 until parsedData.size - 1) {
            for(j in i + 1 until parsedData.size) {
                val first = parsedData[i]
                val second = parsedData[j]
                if(first isConnected second) {
                    first.addConnection(second)
                    second.addConnection(first)
                }
            }
        }
        return parsedData.fold(0) { acc, value ->
            acc + value.allWalls()
        }
    }

    override fun part2(parsedData: List<Coordinates>): Int {
        val droplets = mutableMapOf<Coordinates, Boolean>()
        for(i in 0 until parsedData.size - 1) {
            for(j in i + 1 until parsedData.size) {
                val first = parsedData[i]
                val second = parsedData[j]
                if(first isConnected second) {
                    first.addConnection(second)
                    second.addConnection(first)
                }
            }
        }
        return 0
    }

    override fun parseInput(input: String, step: Int): List<Coordinates> =
        input
            .lines()
            .map { line ->
                line
                    .split(",")
                    .let {
                        Coordinates(it[0].toInt(), it[1].toInt(), it[2].toInt())
                    }
            }

    data class Coordinates(
        val x: Int,
        val y: Int,
        val z: Int
    ) {
        private val connections = mutableSetOf<Coordinates>()

        fun addConnection(coords: Coordinates) {
            connections.add(coords)
        }

        fun allWalls() =
            6 - connections.size


    }

    infix fun Coordinates.isConnected(another: Coordinates): Boolean =
        sqrt(
            (this.x - another.x).toDouble().pow(2) +
                    (this.y - another.y).toDouble().pow(2) +
                    (this.z - another.z).toDouble().pow(2)
        ) <= 1.0
}