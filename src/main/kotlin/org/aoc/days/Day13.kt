package org.aoc.days

import org.aoc.data.AoC
import java.lang.StringBuilder
import java.util.UUID

data class Signal(
    val parent: Signal? = null,
    var children: Signal? = null,
    var nextValue: Signal? = null
) {
    val uuid = UUID.randomUUID()

    var value: Int? = null

    fun hasValue() =
        value != null

    fun addChildren(): Signal =
        Signal(this).also {
            this.children = it
        }

    fun addNextValue(): Signal =
        Signal(parent).also {
            this.nextValue = it
        }

    fun setValue(value: Int?): Signal {
        this.value = value
        return this
    }

    fun wrap(): Signal {
        val hold = value ?: return this
        value = null
        return this.apply {
            addChildren().setValue(hold)
        }
    }

    fun getParentIfSet(): Signal =
        parent ?: this

    fun packets(): List<Signal> {
        val l = mutableListOf<Signal>()
        l.add(this)
        var s = this
        while(s.nextValue != null) {
            s = s.nextValue!!
            l.add(s)
        }
        return l
    }

    override fun toString(): String {
        if(children != null) return "[$children"
        if(value != null) {
            return if(nextValue != null) {
                "$value,$nextValue"
            } else {
                "$value"
            }
        }
        return "]"
    }
}

enum class SignalProperly(val value: Boolean?) {
    TRUE(true), FALSE(false), KEEP_GOING(null)
}

object SignalComparator {

    fun isProperlyPacket(first: Signal, second: Signal): Boolean {
        val f = first.children!!.packets()
        val s = second.children!!.packets()
        val packetsCompared = comparePackets(f, s)
        return packetsCompared.value ?: true
    }

    private fun comparePackets(first: List<Signal>, second: List<Signal>): SignalProperly {
        val size = listOf(first.size, second.size).min()
        for(i in 0 until size) {
            var f = first[i]
            var s = second[i]
            if(f.hasValue() && s.hasValue()) {
                val fv = f.value ?: 0
                val sv = s.value ?: 0
                if(fv < sv) return SignalProperly.TRUE
                else if(fv > sv) return SignalProperly.FALSE
                continue
            }
            if(!f.hasValue() && !s.hasValue()) {
                val fc = f.children
                val sc = s.children
                if(fc == null && sc == null) continue
                else if(fc == null) return SignalProperly.TRUE
                else if (sc == null) return SignalProperly.FALSE
                when (val nc = comparePackets(fc.packets(), sc.packets())) {
                    SignalProperly.KEEP_GOING -> continue
                    else -> return nc
                }
            }
            if(f.hasValue() != s.hasValue()) {
                f = f.wrap()
                s = s.wrap()
                when (val pc = comparePackets(listOf(f), listOf(s))) {
                    SignalProperly.KEEP_GOING -> continue
                    else -> return pc
                }
            }
        }
        return when {
            first.size > second.size -> SignalProperly.FALSE
            first.size < second.size -> SignalProperly.TRUE
            else -> SignalProperly.KEEP_GOING
        }
    }
}

class Day13 : AoC<List<Signal>, Int, Int>() {
    override fun part1(parsedData: List<Signal>): Int {
        var index = 0
        return parsedData
            .mapIndexedNotNull { index, signal ->
                if(index % 2 == 0)
                    signal to parsedData[index + 1]
                else
                    null
            }
            .fold(0) { acc, value ->
                ++index
                val multiplier = if(SignalComparator.isProperlyPacket(value.first, value.second)) 1 else 0
                acc + index * multiplier
            }
    }

    override fun part2(parsedData: List<Signal>): Int {
        val allSignals = parsedData.toMutableList()
        val f = Signal().apply {
            addChildren().addChildren().setValue(2)
        }
        val s = Signal().apply {
            addChildren().addChildren().setValue(6)
        }
        allSignals.add(f)
        allSignals.add(s)
        val signalsWithLowerLevel = mutableListOf<Pair<Signal, Int>>()
        for(i in allSignals.indices) {
            var lowerBy = 0
            for(j in allSignals.indices) {
                if(i != j) {
                    val lower = SignalComparator.isProperlyPacket(allSignals[i], allSignals[j])
                    if(lower) lowerBy++
                }
            }
            signalsWithLowerLevel.add(allSignals[i] to lowerBy)
        }
        var index = 0
        return signalsWithLowerLevel
            .sortedByDescending { it.second }
            .fold(1) { acc, value ->
                ++index
                val multiplier = if(value.first.uuid == f.uuid || value.first.uuid == s.uuid) index else 1
                acc * multiplier
            }
    }

    override fun parseInput(input: String, step: Int): List<Signal> {
        val signals = mutableListOf<Signal>()
        val lines = input.split("\n")
        for (i in lines.indices step 3) {
            val firstSignal = lines[i]
            val secondSignal = lines[i + 1]
            signals.add(firstSignal.toSignal())
            signals.add(secondSignal.toSignal())
        }
        return signals
    }

    private fun String.toSignal(): Signal {
        var signal = Signal()
        var concat = StringBuilder()
        for (i in this.indices) {
            val value = this[i]
            if((value == ']' || value == ',') && concat.isNotEmpty()) {
                signal.setValue(concat.toString().toInt())
                concat = StringBuilder()
            }
            when (value) {
                '[' -> signal = signal.addChildren()
                ']' -> signal = signal.getParentIfSet()
                ',' -> signal = signal.addNextValue()
                else -> concat.append(value)
            }
        }
        return signal
    }

}