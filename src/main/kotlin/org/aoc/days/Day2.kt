package org.aoc.days

import org.aoc.data.AoC

class Day2: AoC<List<Any>, Int, Int>() {
    override fun part1(parsedData: List<Any>): Int {
        return -1
    }

    override fun part2(parsedData: List<Any>): Int {
        return -1
    }

    override fun parseInput(input: String, step: Int): List<Any> {
        return listOf()
    }

}