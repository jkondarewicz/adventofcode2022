package org.aoc.data

import org.aoc.PerformanceResult

class Solution<P1, P2>(
    private val day: Int,
    val part1: PerformanceResult<P1>,
    val part2: PerformanceResult<P2>
) {
    fun printSolution() {
        println("Day$day part1 took ${part1.executionTime}ms")
        println("${part1.solution}")
        println()
        println("Day$day part2 took ${part2.executionTime}ms")
        println("${part2.solution}")
        println()
    }
}